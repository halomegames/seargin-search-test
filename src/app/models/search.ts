import { Observable } from 'rxjs';

export interface ISearchService<T> {
  search$(query: string): Observable<T[]>;
  load$(): Promise<boolean>;
  mapToSearchResult(model: T): SearchResultModel;
}

export enum SearchType {
  USER = 'user',
  MERCHANT = 'merchant'
}

export enum SearchStatus {
  NONE,
  IS_SEARCHING,
  HAS_RESULTS,
  NO_RESULTS,
  ERROR_OCCURED
}

export class SearchResultModel {
  constructor(
    readonly type: SearchType,
    readonly name: string,
    readonly id: string
  ) {}
}

export default ISearchService;
