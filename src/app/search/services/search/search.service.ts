import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { sortBy } from 'sort-by-typescript';
import ISearchService, { SearchResultModel } from 'src/app/models/search';
import { SearchEnginesProviderService } from '../../../services/searchEnginesProvider/searchEnginesProvider.service';

@Injectable({ providedIn: 'root' })
export class SearchService {
  constructor(private searchEngines: SearchEnginesProviderService) {}

  public loadAll() {
    this.searchEngines.getSearchEngines().map(engine => engine.load$());
  }

  public search$(query: string): Observable<SearchResultModel[]> {
    return forkJoin(
      this.searchEngines
        .getSearchEngines()
        .map((service: ISearchService<any>) =>
          service
            .search$(query)
            .pipe(map(s => s.map(service.mapToSearchResult)))
        )
    ).pipe(
      map(result => [].concat(...result)),
      map(result => result.sort(sortBy('name')))
    );
  }
}
