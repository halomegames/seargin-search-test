import { Component, OnInit } from '@angular/core';
import { SearchResultModel, SearchStatus } from 'src/app/models/search';
import { SearchService } from '../services/search/search.service';
import { ExportToCsvService } from '../../services/exportToCsv/exportToCsv.service';
import * as moment from 'moment';
import { DateService } from 'src/app/services/date/date.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public currentSearchStatus: SearchStatus;
  public searchStatus = SearchStatus;
  public searchResult: SearchResultModel[];

  constructor(
    private searchService: SearchService,
    private dateService: DateService,
    private exportToCsv: ExportToCsvService
  ) {}

  ngOnInit() {
    this.searchService.loadAll();
  }

  executeSearch(searchValue: string) {
    this.clearSearch();

    if (searchValue === '') {
      return;
    }

    this.currentSearchStatus = SearchStatus.IS_SEARCHING;

    this.searchService.search$(searchValue).subscribe(
      (result: SearchResultModel[]) => {
        this.searchResult = result;
        this.currentSearchStatus =
          result.length > 0
            ? SearchStatus.HAS_RESULTS
            : SearchStatus.NO_RESULTS;
      },
      () => {
        this.handleError();
      }
    );
  }

  executeSave(searchValue: string) {
    const fileName = `${searchValue}-${this.dateService.getFormattedToday()}`;

    this.exportToCsv.export(
      fileName,
      this.searchResult.map(user => ({
        Type: user.type.toString(),
        Name: user.name,
        Identifier: user.id
      }))
    );
  }

  handleError() {
    this.clearSearch();
    this.currentSearchStatus = SearchStatus.ERROR_OCCURED;
  }

  clearSearch() {
    this.searchResult = [] as SearchResultModel[];
    this.currentSearchStatus = SearchStatus.NONE;
  }
}
