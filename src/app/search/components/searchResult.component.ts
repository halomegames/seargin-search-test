import { Component, Input } from '@angular/core';
import { SearchResultModel } from '../../models/search';

@Component({
  selector: 'app-search-result',
  templateUrl: 'searchResult.component.html'
})
export class SearchResultComponent {
  @Input() searchResult: SearchResultModel[];

  constructor() {}
}
