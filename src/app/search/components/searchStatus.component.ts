import { Component, Input } from '@angular/core';
import { SearchStatus } from 'src/app/models/search';

@Component({
  selector: 'app-search-status',
  templateUrl: 'searchStatus.component.html'
})
export class SearchStatusComponent {
  @Input() status: SearchStatus;
  public searchStatus = SearchStatus;

  constructor() {}
}
