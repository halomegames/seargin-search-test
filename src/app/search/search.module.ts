import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './components/search.component';
import { SearchService } from './services/search/search.service';
import { SearchStatusComponent } from './components/searchStatus.component';
import { SearchResultComponent } from './components/searchResult.component';

@NgModule({
  declarations: [SearchComponent, SearchStatusComponent, SearchResultComponent],
  imports: [CommonModule],
  exports: [SearchComponent, SearchStatusComponent, SearchResultComponent],
  providers: [SearchService]
})
export class SearchModule {}
