import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user';
import { Observable } from 'rxjs';
import { UserRepository } from '@search-app/data';
import ISearchService, { SearchResultModel, SearchType } from 'src/app/models/search';

@Injectable({ providedIn: 'root' })
export class UserService implements ISearchService<User>{

  constructor(private repo: UserRepository) { }

  search$(query: string): Observable<User[]> {
    return this.repo.search$(query);
  }

  load$(): Promise<boolean> {
    return this.repo.load$();
  }

  mapToSearchResult(user: User): SearchResultModel {
    return new SearchResultModel(SearchType.USER, user.name, user.personId);
  }
}
