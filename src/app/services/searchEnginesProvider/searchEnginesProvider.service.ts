import { Injectable } from '@angular/core';
import ISearchService from 'src/app/models/search';
import { UserService } from 'src/app/services/user/user.service';
import { MerchantService } from 'src/app/services/merchant/merchant.service';

@Injectable({ providedIn: 'root' })
export class SearchEnginesProviderService {
  constructor(
    public userService: UserService,
    public merchantService: MerchantService
  ) {}

  public getSearchEngines(): ISearchService<any>[] {
    return [this.userService, this.merchantService];
  }
}
