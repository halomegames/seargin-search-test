import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Merchant } from 'src/app/models/merchant';
import { MerchantRepository } from '@search-app/data';
import ISearchService, {
  SearchResultModel,
  SearchType
} from 'src/app/models/search';

@Injectable({
  providedIn: 'root'
})
export class MerchantService implements ISearchService<Merchant> {
  constructor(private repo: MerchantRepository) {}

  load$(): Promise<boolean> {
    return this.repo.load$();
  }

  search$(query: string): Observable<Merchant[]> {
    return this.repo.search$(query);
  }

  mapToSearchResult(user: Merchant): SearchResultModel {
    return new SearchResultModel(SearchType.MERCHANT, user.name, user.vat);
  }
}
