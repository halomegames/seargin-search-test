import { Injectable } from '@angular/core';
import exportFromJSON from 'export-from-json';

@Injectable({
  providedIn: 'root'
})
export class ExportToCsvService {
  constructor() {}

  public export(fileName: string, data: any[]) {
    exportFromJSON({
      data,
      fileName,
      exportType: 'csv'
    });
  }
}
