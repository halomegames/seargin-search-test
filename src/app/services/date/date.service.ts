import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  private COMMON_DATE_FORMATTER = 'YY-MM-DD';

  public commonFormat(date: moment.Moment) {
    return moment(date).format(this.COMMON_DATE_FORMATTER);
  }

  public getFormattedToday(): string {
    return this.commonFormat(moment(moment.now())).toString();
  }
}
